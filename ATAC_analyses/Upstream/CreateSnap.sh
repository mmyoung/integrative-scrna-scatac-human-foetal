# sort filtered fragments 
sort -k4,4 merged.filt.sort.bed > merged.filt.sort.RG.bed
# make snap object
snaptools snap-pre  \
        --input-file=merged.filt.sort.RG.bed.gz  \
        --output-snap=merged.snap  \
        --genome-name=hg38  \
        --genome-size=hg38.chrom.sizes  \
        --min-mapq=30  \
        --min-flen=50  \
        --max-flen=1000  \
        --keep-chrm=TRUE  \
        --keep-single=FALSE  \
        --keep-secondary=False  \
        --overwrite=True  \
        --max-num=20000  \
        --min-cov=500  \
        --verbose=True
# add bin matrix to the snap object
snaptools snap-add-bmat \
    --snap-file=merged.snap \
    --bin-size-list 5000 10000 \
    --verbose=True
